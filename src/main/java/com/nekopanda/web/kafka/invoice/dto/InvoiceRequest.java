package com.nekopanda.web.kafka.invoice.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvoiceRequest {
    private String debitur;
    private String description;
    private BigDecimal amount;
}
