package com.nekopanda.web.kafka.invoice.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class InvoiceResponse {
    private Boolean success;
    private String invoiceNumber;
    private String debitur;
    private String description;
    private BigDecimal amount;
}
