package com.nekopanda.web.kafka.invoice.service;

import com.nekopanda.web.kafka.invoice.dto.InvoiceRequest;
import com.nekopanda.web.kafka.invoice.dto.InvoiceResponse;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class InvoiceService {
    private static Long nomerInvoiceTerakhir = 1L;

    @Autowired
    private KafkaSenderService kafkaSenderService;

    public void createInvoice(InvoiceRequest invoiceRequest){
        InvoiceResponse response = new InvoiceResponse();
        BeanUtils.copyProperties(invoiceRequest, response);
        response.setInvoiceNumber(generateInvoiceNumber());
        response.setSuccess(true);
        kafkaSenderService.kirimInvoiceResponse(response);

    }

    private static String generateInvoiceNumber(){
        String dateTime = LocalDate.now().format(DateTimeFormatter.ISO_DATE);
        String invoiceNumber = String.format("%05d", nomerInvoiceTerakhir);
        nomerInvoiceTerakhir = nomerInvoiceTerakhir + 1;
        return (dateTime + invoiceNumber).replace("-", "");
    }
}
