package com.nekopanda.web.kafka.invoice.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nekopanda.web.kafka.invoice.dto.InvoiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaSenderService {
    @Value("${kafka.topic.invoice-response}")
    private String topicInvoiceResponse;

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaSenderService.class);

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public void kirimInvoiceResponse(InvoiceResponse invoiceResponse) {
        try {
            LOGGER.info("Invoice Response : {}", invoiceResponse);
            String msg = objectMapper.writeValueAsString(invoiceResponse);
            LOGGER.info("JSON Invoice Response : {}", msg);
            kafkaTemplate.send(topicInvoiceResponse, msg);
        } catch (Exception err) {
            LOGGER.error(err.getMessage(), err);
        }
    }

}
